## function for setting mask
'''
NW N  NE
W     E
SW S  SE

8 direction
'''



W = 0
NW = 1
N = 2
NE = 3
E = 4
SE= 5
S = 6
SW = 7



'''
mask N & mask S
1111
1111
1111
'''
def maskN(rows, cols):
	res = 1 << rows*cols
	return res - 1

def maskS(rows, cols):
	return maskN(rows, cols)


'''
mask W
11110
11110
11110
'''
def maskW(rows, cols):
	res = maskN(rows,cols)
	for x in range(rows):
		res -= (1 << x * cols)
	return res

'''
mask E

01111
01111
01111
'''
def maskE(rows, cols):
	res = maskN(rows, cols)
	for x in range(1, rows + 1):
		res -= (1 << x*cols - 1)
	return res


'''
mask NW

11110
11110
00000
'''

def maskNW(rows, cols):
	res = maskW(rows - 1, cols)
	return res << cols


'''
mask NE
01111
01111
00000
'''
def maskNE(rows, cols):
	res = maskE(rows - 1, cols)
	return res << cols


'''
mask SE
00000
01111
01111
'''
def maskSE(rows, cols):
	res = maskE(rows - 1, cols)
	return res

'''
mask SW
00000
11110
11110
'''
def maskSW(rows, cols):
	res = maskW(rows - 1, cols)
	return res

'''
NW N  NE
W     E
SW S  SE

W = 0
NW = 1
N = 2
NE = 3

E = 4
SE = 5
S = 6
SW = 7

'''
def mask(rows, cols, i):
	if i == W:
		return maskW(rows, cols)
	elif i == NW:
		return maskNW(rows, cols)
	elif i == N:
		return maskN(rows, cols)
	elif i == NE:
		return maskNE(rows, cols)
	elif i == E:
		return maskE(rows, cols)
	elif i == SE:
		return maskSE(rows, cols)
	elif i == S:
		return maskS(rows, cols)
	else:
		return maskSW(rows, cols)

def create_mask(rows, cols):
	MASK = [0, 0, 0, 0, 0, 0, 0, 0]
	for i in range(8):
		MASK[i] = mask(rows, cols, i)
	
	return MASK


def corner_mask(rows, cols):
	'''
	100001
	000000
	000000
	100001
	'''
	x = (1 << (cols - 1)) + 1
	res = x
	res <<= ((rows - 1) * cols)
	return res + x


def buffer_mask(rows, cols):
	'''
	0100010
	1100011
	0000000
	0000000
	1100011
	0100010
	'''
	x = (1 << (cols - 2)) + 2 # 0100010
	y = (3 << (cols - 2)) + 3 # 1100011
	res = x

	res <<= (rows - 1) * cols
	res += y << ((rows - 2) * cols)
	res += (y << cols)
	res += x
	return res

def edge_mask(rows, cols):
	'''
	0011100
	0000000
	1000001
	1000001
	0000000
	0011100
	7 => 1000
	00111100
	'''
	x = ((1 << (cols - 4)) - 1) << 2 # 0011100
	y = (1 << cols - 1) + 1
	res = x
	res <<= (rows - 1) * cols
	for i in range(2, rows - 2):
		res += (y << i * cols)
	return res + x

def c_weight(rows, cols):
	return rows + cols

def f_weight(rows, cols):
	return -2

def b_weight(rows, cols):
	return -2

def e_weight(rows, cols):
	return 1
	
def create_shift(rows, cols):
	return [1, cols + 1, cols, cols - 1, 1 , cols + 1, cols , cols - 1]


def shifting(disk, direction, SHIFT, MASK):
	if direction < 4:
		return (disk << SHIFT[direction]) & MASK[direction]
	else:
		return (disk >> SHIFT[direction]) & MASK[direction]
