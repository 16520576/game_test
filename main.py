import othello
import settings
import mask
import minimax





def test(rows, cols, depth):
	turn = 0
	x = othello.othello(rows, cols)
	MAX = x.rows*x.cols + 1
	MIN = -MAX

	while x.is_end() == False:
		p, o, k = minimax.pingpong(x, turn, depth)
		if p != 0 and o != 0:
			x.disk[turn] = p
			x.disk[turn^1] = o
			x.print_othello(0)
			print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
			

		turn ^= 1
	black = minimax.counting(x.disk[settings.BLACK])
	white = minimax.counting(x.disk[settings.WHITE])

	if black > white:
		print('{} win: {} point(s)'.format(settings.BLACK, black - white))
	elif black < white:
		print('{} win: {} point(s)'.format(settings.WHITE, white - black))
	else:
		print('draw')


test(10,6, 3)