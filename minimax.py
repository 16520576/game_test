import othello
import mask


def counting(bits):
		n = 0
		while bits:
			bits &= bits - 1
			n += 1
		return n




#get legal move for a p_disk
def legal_move(board, p_disk, o_disk):
	'''
	0 4 ----
	2 6 |
	1 3 5 7 /\
	'''

	empty_disk = ~ (p_disk | o_disk)
	move = 0

	for direction in range(8):
			
		dist = 0
		if direction == 0 or direction == 4:
				dist = board.cols
		elif direction == 2 or direction == 6:
				dist = board.rows
		else:
			dist = min(board.cols, board.rows)

		x = mask.shifting(p_disk, direction, board.SHIFT, board.MASK) & o_disk

		for i in range(dist - 2):
			x |= (mask.shifting(x, direction, board.SHIFT, board.MASK) & o_disk)

		move |= (mask.shifting(x, direction, board.SHIFT, board.MASK) & empty_disk)

	return move


# disks adjacent to empty squares
def frontier_disk(board, p_disk, o_disk):
	empty_disk = ~ (p_disk | o_disk)
	p_frontier = 0
	o_frontier = 0

	for direction in range(8):
		x = mask.shifting(empty_disk, direction, board.SHIFT, board.MASK)
		p_frontier |= (x & p_disk)
		o_frontier |= (x & o_disk)

	return p_frontier, o_frontier

def pos_evaluation(board, p_disk, o_disk):


	corner_mask = mask.corner_mask(board.rows, board.cols)
	buffer_mask = mask.buffer_mask(board.rows, board.cols)
	edge_mask = mask.edge_mask(board.rows, board.cols)

	f_player, f_opponent = frontier_disk(board, p_disk, o_disk)
	

	c_player = p_disk & corner_mask
	c_opponent = o_disk & corner_mask

	b_player = p_disk & buffer_mask
	b_opponent = o_disk & buffer_mask

	e_player = p_disk & edge_mask
	e_opponent = o_disk & edge_mask

	f = mask.f_weight(board.rows, board.cols)
	c = mask.c_weight(board.rows, board.cols)
	b = mask.b_weight(board.rows, board.cols)
	e = mask.e_weight(board.rows, board.cols)

	score_player = c * counting(c_player) - b * counting(b_player)  - f * counting(f_player) + e * counting(e_player)
	score_opponent = c * counting(c_opponent) - b * counting(b_opponent) - f * counting(f_opponent) + e * counting(e_opponent)

	return score_player - score_opponent

def flip(board, p_disk, o_disk, index):
		_mask = 1 << index

		p = p_disk
		o = o_disk

		# push into p_disk
		p |= _mask

		flipping_disk = 0

		for direction in range(8):
			dist = 0
			if direction == 0 or direction == 4:
				dist = board.cols
			elif direction == 2 or direction == 6:
				dist = board.rows
			else:
				dist = min(board.cols, board.rows)

			x = mask.shifting(_mask, direction, board.SHIFT, board.MASK) & o_disk
			
			for i in range(dist):
				x |= mask.shifting(x, direction, board.SHIFT, board.MASK) & o_disk

			bounding = mask.shifting(x, direction, board.SHIFT, board.MASK) & p_disk

			if bounding != 0:
				flipping_disk |= x

		p ^= flipping_disk
		o ^= flipping_disk

		return p, o


def minimax(board, p_disk, o_disk, state, depth, alpha, beta):
	p_move = legal_move(board, p_disk, o_disk)
	o_move = legal_move(board, o_disk, p_disk)
	
	if (p_move == 0 and o_move == 0) or depth == 0:
		return pos_evaluation(board, p_disk, o_disk)

	if p_move == 0 and o_move:
		return minimax(board, p_disk, o_disk, state^1, depth - 1, alpha, beta)

	if state == 0:
		
		best = - (board.rows * board.cols + 1)

		x = p_move
		index = 0
		#1011110000
		while x:
			if ((x >> 1) << 1) != x:
				
				p, o = flip(board, p_disk, o_disk, index)

				val = minimax(board, p, o, state ^ 1, depth - 1, alpha, beta)

				best = max(best, val)
				alpha = max(alpha, best)

				if beta <= alpha:
					break

			index += 1
			x >>= 1
		return best

	if state == 1:
		best = board.rows * board.cols + 1
		x = o_move
		index = 0
		while x:
			if ((x >> 1) << 1) != x:
				o, p = flip(board, o_disk, p_disk, index)

				val = minimax(board, p, o, state ^ 1, depth - 1, alpha, beta)

				best = min(best, val)
				beta = min(beta, best)

				if beta <= alpha:
					break

			index += 1
			x >>= 1
		return best


def pingpong(board, state, depth):
		
	legal = legal_move(board, board.disk[state], board.disk[state^1])
	MIN = - (board.rows * board.cols + 1)
	MAX = - MIN
	best = []

	p_disk = []
	o_disk = []

	i = legal


	index = 0

	while i:
		if ((i >> 1) << 1) != i:
			p, o = flip(board, board.disk[state], board.disk[state^1], index)
			b = minimax(board, board.disk[state], board.disk[state^1], state ^ 1, 3, MIN, MAX)
			best += [b]

			p_disk += [p]
			o_disk += [o]

		index += 1
		i >>= 1
	if best != []:
		max_index = best.index(max(best))

		return p_disk[max_index], o_disk[max_index], max_index

	return 0, 0, 0


