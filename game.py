import pygame
import settings
import othello
import minimax

pygame.init()

ROW = 8
COL = 8

DEPTH = 3
WEIGHT = 1000
HEIGHT = 600
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
YELLOW = (255, 255, 0)
BLUE = (0, 0, 255)

mainmenu = pygame.image.load("Image/mainmenu.png")
playing = pygame.image.load('Image/playing.png')
black = pygame.image.load('Image/black.png')
white = pygame.image.load('Image/white.png')

screen = pygame.display.set_mode((WEIGHT, HEIGHT))
pygame.display.set_caption('hello world')

clock = pygame.time.Clock()

crashed = False


#black disk for player
class Disk(pygame.sprite.Sprite):
	
	#type is black or white
	#size_rect is the size of disk

	def __init__(self, size_rect, location, type):
		pygame.sprite.Sprite.__init__(self)

		self.image = pygame.Surface((size_rect, size_rect))
		self.image.fill(YELLOW)
		pygame.draw.rect(self.image, BLACK, [0, 0, size_rect, size_rect], 2)
		pygame.draw.circle(self.image,type, [int(size_rect / 2), int(size_rect / 2)], int((size_rect - 4 )/ 2))
		self.rect = self.image.get_rect()

		self.rect.left, self.rect.top = location

class Background_playing:
	row = 0
	col = 0
	size_rect = 0

	image = pygame.Surface((WEIGHT, HEIGHT))
	image.fill(BLUE)

	def put_disk(self, pos, type):
		
		x, y = pos

		pygame.draw.circle(self.image,type, [int(self.size_rect * (x + 0.5)), int(self.size_rect*(y + 0.5))], int((self.size_rect - 4 )/ 2))


	def draw_disk(self, disk, type):
		sim = disk
		p = 0

		while sim:
			if sim & 1:
				
				y = int(p / self.col)
				x = p % self.col

				y = self.row - y - 1
				x = self.col - x - 1

				self.put_disk((x,y), type)
			p += 1
			sim >>= 1


	def __init__(self, row, col):
		self.row = row
		self.col = col

		d1 = float(WEIGHT / col)
		d2 = float(HEIGHT / row)

		size_of_rect = min(d1, d2)

		self.size_rect = int(size_of_rect)
		pygame.draw.rect(self.image, YELLOW, [0, 0, col*self.size_rect, row*self.size_rect])
		
		board = othello.othello(row, col)
		
		for n in range(col + 1):
			pygame.draw.line(self.image, BLACK, [n * self.size_rect, 0], [n*self.size_rect, row*self.size_rect])

		for n in range(row + 1):
			pygame.draw.line(self.image, BLACK, [0, n*self.size_rect], [col*self.size_rect, n*self.size_rect])

		
		
		self.draw_disk(board.disk[settings.BLACK], BLACK)
		self.draw_disk(board.disk[settings.WHITE], WHITE)
	

def get_pos(BackGround, pos):
	x, y = pos
	c = int(x / BackGround.size_rect)
	r = int(y / BackGround.size_rect)

	return c, r

def get_location(BackGround, pos):
	x, y = pos

	c = int(x / BackGround.size_rect)
	r = int(y / BackGround.size_rect)

	return c * BackGround.size_rect, r * BackGround.size_rect



BackGround = Background_playing(ROW, COL)
BackGround_slow = BackGround

size = BackGround.size_rect

board = othello.othello(ROW, COL)
print(bin(2))
print(bin(board.disk[settings.WHITE]))
print(board.disk[settings.BLACK])

TURN = settings.BLACK

b_index = 0

w_index = 0

while not crashed:
	#set FPS = 60
	clock.tick(60)

	#input events
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			crashed = True
		if board.is_end() == True:
			crash = True
			black = minimax.counting(board.disk[settings.BLACK])
			white = minimax.counting(board.disk[settings.WHITE])
			
			if black > white:
				print('You win : {} point(s)'.format(black - white))
			elif black < white:
				print('Computer win: {} point(s)'.format(white - black))
			else:
				print('draw')

		if event.type == pygame.MOUSEBUTTONDOWN and TURN == settings.BLACK:
			x, y = get_pos(BackGround, event.pos)
			
			x = COL - 1 - x
			y = ROW - 1 - y


			move = 1 << (y * COL + x)
			b_index = move
			#check if it legal
			legal_move = minimax.legal_move(board, board.disk[settings.BLACK], board.disk[settings.WHITE])

			if legal_move:
				if move & legal_move:
					BackGround.draw_disk(move, BLACK)

					b_disk, w_disk = minimax.flip(board, board.disk[settings.BLACK], board.disk[settings.WHITE], y*COL + x)
					board.disk[settings.BLACK]=b_disk
					board.disk[settings.WHITE]=w_disk

					

					TURN ^= 1
			else:
				TURN ^= 1


		

			#diskx = Disk(BackGround.size_rect, pos, WHITE)
			
			#set
			#adasodi

			#add

			
		
		#print(event)
		pygame.display.update()


	if TURN == settings.WHITE:
		


		w_legal_move = minimax.legal_move(board, board.disk[settings.WHITE], board.disk[settings.BLACK])
			
		if w_legal_move:
			w_disk, b_disk, index = minimax.pingpong(board, settings.WHITE, DEPTH)
					
			board.disk[settings.BLACK]=b_disk
			board.disk[settings.WHITE]=w_disk
		TURN ^= 1
	
	
	BackGround.draw_disk(board.disk[settings.BLACK], BLACK)
	BackGround.draw_disk(board.disk[settings.WHITE], WHITE)
	#draw
	screen.blit(BackGround.image, (0, 0))
	
pygame.quit()
quit()