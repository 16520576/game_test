import settings
import mask
import minimax

class othello:
	#number of rows and collums
	rows = 0
	cols = 0

	disk = [0, 0]
	
	# Mask for shifting bitboard
	MASK = [0, 0, 0, 0, 0, 0, 0, 0]
	SHIFT = [0, 0, 0, 0, 0, 0, 0, 0]

	def get_point(self, row, col):
		mask = 1 << (row * self.cols + col)

		if (self.disk[settings.BLACK] & mask):
			return settings.BLACK 
		if (self.disk[settings.WHITE] & mask):
			return settings.WHITE
		return settings.EMPTY

	def set_point(self, row, col, state):
		mask = 1 << (row * self.cols + col)
		self.disk[settings.BLACK] &= (~mask)
		self.disk[settings.WHITE] &= (~mask)

		self.disk[state] |= mask

	def del_point(self, row, col, state):
		if self.get_point(row, col) == state:
			mask = 1 << (row * self.cols + col)
			self.disk[state] -= mask

	def __init__(self, rows, cols):
		self.rows = rows
		self.cols = cols
		
		self.MASK = mask.create_mask(rows, cols)
		self.SHIFT = mask.create_shift(rows, cols)

		self.set_point(int(rows/2), int(cols/2), settings.BLACK)
		self.set_point(int(rows/2) - 1, int(cols/2) - 1, settings.BLACK)
		self.set_point(int(rows/2), int(cols/2) - 1, settings.WHITE)
		self.set_point(int(rows/2) - 1, int(cols/2), settings.WHITE)


	def is_end(self):
		b_move = minimax.legal_move(self, self.disk[settings.BLACK], self.disk[settings.WHITE])
		w_move = minimax.legal_move(self, self.disk[settings.WHITE], self.disk[settings.BLACK])
		
		if (b_move == 0 and w_move == 0):
			return True
		return False

	
	def print_othello(self, next_move):
		for i in range(self.rows):
			for j in range(self.cols):
				mask = 1 << (i*self.cols + j)

				if self.disk[settings.BLACK] & mask:
					print("0 ", end = "")
				elif self.disk[settings.WHITE] & mask:
					print("1 ", end = "")
				elif next_move & mask:
					print("x ", end = "")
				else:
					print("- ", end = "")
			print("")


